import React, { useState, useEffect } from 'react';
import axios from 'axios';
import JobList from './components/JobList';

const App: React.FC = () => {
  const [jobs, setJobs] = useState([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    const fetchJobs = async () => {
      const response = await axios.get(`http://localhost:5000/api/jobs?page=${page}`);
      console.log(response.data);
      setJobs(response.data.jobs.ads);
    };
    fetchJobs();
  }, [page]);

  return (
    <div className="App">
      <h1>Offres d'emploi à Bordeaux</h1>
      <JobList jobs={jobs} />
      <div>
        <button onClick={() => setPage(page - 1)} disabled={page === 1}>Previous</button>
        <button onClick={() => setPage(page + 1)}>Next</button>
      </div>
    </div>
  );
};

export default App;
