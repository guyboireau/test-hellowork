import React from 'react';

const JobItem: React.FC<{ job: any }> = ({ job }) => {
  return (
    <div className="job-item">
        <h2 dangerouslySetInnerHTML={{ __html: job.title }} />
        <p dangerouslySetInnerHTML={{ __html: job.description }} />
        <a href={job.link} target="_blank" rel="noopener noreferrer">Postuler</a>
    </div>
  );
};

export default JobItem;
