import React from 'react';
import JobItem from './JobItem';

const JobList: React.FC<{ jobs: any[] }> = ({ jobs }) => {
  return (
    <div>
      {jobs.map((job) => (
        <JobItem key={job.id} job={job} />
      ))}
    </div>
  );
};

export default JobList;
