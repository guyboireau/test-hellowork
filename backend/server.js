const express = require('express');
const axios = require('axios');
const cors = require('cors');
require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

let authToken = null;

const authenticate = async () => {
    try {
        const response = await axios.post('https://api.jobijoba.com/v3/fr/login', {
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET
        });
        authToken = response.data.token;
        console.log('Token fetched:', authToken);
    } catch (error) {
        console.error('Error fetching token:', error.response ? error.response.data : error.message);
    }
};

authenticate();

setInterval(authenticate, 55 * 60 * 1000);

app.get('/api/jobs', async (req, res) => {
    if (!authToken) {
        return res.status(500).json({ error: 'Failed to fetch jobs, no auth token' });
    }

    const page = parseInt(req.query.page) || 5;
    const limit = parseInt(req.query.limit) || 20;

    try {
        const response = await axios.get('https://api.jobijoba.com/v3/fr/ads/search', {
            params: {
                where: 'Bordeaux',
                limit: limit,
                page: page
            },
            headers: {
                Authorization: `Bearer ${authToken}`
            }
        });

        res.json({
            jobs: response.data.data,
            currentPage: page,
            totalPages: Math.ceil(response.data.total / limit)
        });
    } catch (error) {
        console.error('Error fetching jobs:', error.response ? error.response.data : error.message);
        res.status(500).json({ error: 'Failed to fetch jobs' });
    }
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
