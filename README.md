
# Test technique HelloWork Guy Boireau

Objectif:

- Récupérer les offres emploi sur la ville de Bordeaux à partir de l'API de Jobijoba

- Proposer une page (stylisée avec un peu de CSS) qui présente ces offres et permet d’y postuler (via l’URL de redirection)
- Proposer une pagination

## Installation

Install test-hellowork with docker

```bash
  git clone https://gitlab.com/guyboireau/test-hellowork.git
  cd test-hellowork
  docker compose up --build
```